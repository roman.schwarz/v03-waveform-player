import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;

import utils.AudioPlayerEngine;

public class Model {
	
	
	public static final int BUFFER_SIZE = 44100;
	public int frequency = 500;
	public int[] samples = new int[BUFFER_SIZE];
	public double period;
	private AudioPlayerEngine audioPlayer;
	
	public Model()	{
		try {
			audioPlayer = new AudioPlayerEngine();
		} catch (LineUnavailableException e) {
			e.printStackTrace();
		}
	}

	
	public void setFrequency(int frequency)	{
		this.frequency = frequency;
		
		double soundCardFreq = 44100;
		period = BUFFER_SIZE / soundCardFreq;

		for (int i = 0; i < samples.length; i++) {
			double t = i / soundCardFreq;
			samples[i] = generateSin(t, this.frequency);

		}
	}
	
	private int generateSin(double t, double freq) {

		double w = 2 * Math.PI * freq;
		return (int) (Integer.MAX_VALUE / 4 * Math.sin(w * t));
		
		
		/*
		 * int maxAmplitude = (Integer.MAX_VALUE - Integer.MIN_VALUE) / 2; int[] sinY =
		 * new int[(int) t]; double w = 2 * Math.PI * freq;
		 * 
		 * for (int i = 0; i < t; i++) { sinY[i] = (int) (maxAmplitude * Math.sin(w *
		 * i)); }
		 * 
		 * return sinY;
		 */
	}

	public void play() {
		try {
			audioPlayer.play(samples);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
