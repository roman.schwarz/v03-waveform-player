import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import utils.AudioPlayerEngine;

@SuppressWarnings("serial")
public class View extends JPanel {
	int numSamples;
	int sldFreq;

	
	private Controller controller;
	
	public View(Controller controller) {
		this.controller = controller;
	}

	private static final int SHOW_SAMPLES = 1000;
	private PlotPanel plotPanel = new PlotPanel(Integer.MIN_VALUE, Integer.MAX_VALUE, SHOW_SAMPLES);;
	private JLabel lblFreq = new JLabel();
	private JSlider sldSetFreq = new JSlider(50, 20000);
	private JButton btnPlay = new JButton("Play");

	private final GridBagConstraints pnlPlotLayout = new GridBagConstraints(1, 3, 1, 1, 1, 1, GridBagConstraints.CENTER,
			GridBagConstraints.BOTH, UiConstants.insets, 0, 0);
	private final GridBagConstraints lblFreqLayout = new GridBagConstraints(1, 1, 1, 1, 0, 0, GridBagConstraints.CENTER,
			GridBagConstraints.NONE, UiConstants.insets, 0, 0);
	private final GridBagConstraints sldSetFreqLayout = new GridBagConstraints(1, 2, 1, 1, 1, 0,
			GridBagConstraints.CENTER, GridBagConstraints.BOTH, UiConstants.insets, 0, 0);
	private final GridBagConstraints btnPlayLayout = new GridBagConstraints(1, 4, 1, 1, 0, 0, GridBagConstraints.CENTER,
			GridBagConstraints.NONE, UiConstants.insets, 0, 0);

	/**
	 * Run init code here (the view has valid dimensions at this point)
	 */
	public void init() {
		setLayout(new GridBagLayout());
		add(plotPanel, pnlPlotLayout);
		add(lblFreq, lblFreqLayout);
		add(sldSetFreq, sldSetFreqLayout);
		add(btnPlay, btnPlayLayout);
		
		btnPlay.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				controller.play();
			}
		});

		sldSetFreq.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				sldFreq = sldSetFreq.getValue();
				controller.setFrequency(sldFreq);
				

			}
		});



	}
	
	public void update(Model model)	{
		//Update UI State according to model
		
		plotPanel.update(model.samples);
		sldSetFreq.setValue(model.frequency);
		lblFreq.setText(String.format("Frequency %d Hz", model.frequency));
	//	plotPanel.setLabelText("Time Period" + period + "[s]");
		
	}





}
