
public class Controller {
	
	private Model model;
	private View view;

	public Controller(Model model) {
		super();
		this.model = model;
	}

	public void setView(View view) {
		this.view = view;
		model.setFrequency(1000);
		view.update(model);
	}

	public void setFrequency(int sldFreq) {
		model.setFrequency(sldFreq);
		view.update(model);
		
	}

	public void play() {
		model.play();
	}
	

}
